from numpy import roll, size, fliplr, tril, triu, conj, ceil, outer, dot, sqrt
import numpy
from numpy.fft import fft, ifft, ifftshift, fftshift
from scipy.sparse.linalg import svds
from numpy.linalg import norm

def makePulse(electricFROG, lastPulse, whichMethod):

    N = size(electricFROG, 1)

    #Do the exact inverse of the procedure in makeFROG...
    #electricFROG = fftshift(fft(electricFROG, axis=0),axes=0)
    #electricFROG = ifft(ifftshift(electricFROG,axes=0),axis=0)
    
    #electricFROG = fftshift(fft(ifftshift(electricFROG,axes=0), axis=0),axes=0)
    electricFROG = fftshift(ifft(ifftshift(electricFROG,axes=0), axis=0),axes=0)
    #At this stage the EF is in time domain

    if (whichMethod == 'Vanilla'):
        #Kane1999 The original FROG inversion algorithm integrated the time domain FROG trace,
        #with respect to the time delay, to obtain subsequent guesses
        #for (so-called “vanilla” or “basic” algorithm) [4], [15],
        #[21]. The integration effectively reduces the gate function
        #to a constant, yielding where is the integration
        #constant and is the next guess for the electric field.
        #While fast, this algorithm stagnates easily and fails to invert
        #spectrograms of double pulses [4], [15], [21].
        outputPulse = numpy.sum(electricFROG,axis=1)
        # normalize so peak height is 1
        outputPulse = outputPulse/numpy.max(numpy.abs(outputPulse))
    #PCGP method
    else:
        #Undo the line:
        #electricFROG = fliplr(fftshift(electricFROG, 1)
        electricFROG = ifftshift(fliplr(electricFROG),1)
        #Undo the lines: 
        #for n in range(1, N):
        #    electricFROG[n, :] = roll(electricFROG[n, :], -n+1)
        for n in range(1, N):
            electricFROG[n, :] = roll(electricFROG[n, :], n-1)
        # Now EF is the "outer product form", see Kane1999.

        # Anti-alias in time domain. See makeFROG for explanation.
        # VK: not clear why it is needed. Hopefully, putting 0s at the perimeter of initial trace is enough.
        #electricFROG = electricFROG - tril(electricFROG, int(-ceil(N/2))) - triu(electricFROG, int(ceil(N/2)));

        if (whichMethod == 'PCGP_Power'): # power method
        #electricFROG*conj(electricFROG.T)
        #outputPulse = lastPulse*(electricFROG@conj(electricFROG.T))
        #tempouter = outer(electricFROG, electricFROG)
        #TODO MEMORY ERROR
            outputPulse = electricFROG@(conj(electricFROG.T)@lastPulse)
            # normalize so peak height is 1
            outputPulse = outputPulse/numpy.max(numpy.abs(outputPulse))
        elif (whichMethod == 'PCGP_SVD'): # SVD method
            [U, S, V] = svds(electricFROG, 1)
            #Normalisation to sqrt(W[0,0]) is valid for second harmonic FROG only (SHG)
            #For third harmonic it is pow1/3
            outputPulse = U[:,0]*sqrt(S[0])   

    return outputPulse
