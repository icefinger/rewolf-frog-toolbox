import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import itertools
from scipy.interpolate import griddata

#See testAPEaxis.ipynb
#During APE FROG aquisition window we've seen that lambdamin=720,lambdamax=880,steps=200.
#The FROG maximum looked to be at 805nm where laser peak frequency is expected.
#FROG WL is half of this, so APE automatically rescales axis labels on FROG!
#In the APE raw data those numbers were written to the header 416.378414 0.378526 and 200 lines are written
#If they are interpreted as wlmax and wlstep then one gets lambdamin=720 applying factor of 2.
#one also gets lambdamax=880 if nbins and not nbins-1 are used.
#So  it looks like APE threats their data as a histogram (and not aquistion with precise frequencies)
#So, one should assume that each data has a frequency in the center of the bin.


def readrawAPE(filename,deltaT=10,nbin=64,debug=True):
    metadata=open(filename).readline().rstrip().split()
    data = np.loadtxt(filename,skiprows=1).T #Transposed for easier unpacking
    timestep = float(metadata[2])
    print("Step time:  ",timestep)
    #raw data grid definition
    trawgrid = np.arange(0,data.shape[0]*timestep,timestep) 
 
    c_light = 2.99792458e8
    #we guessed first that data is wl, but seems this is not correct
    if False:     
        freqmin  = float(metadata[0])
        freqstep = float(metadata[1])
        print("Minimum wl: ",freqmin)
        print("Step wl   : ",freqstep)  
        #lambdarawgrid = np.arange(freqmin, freqmin-(data.shape[1]-1)*freqstep,-freqstep)     #exp max f=398.79155987
        #lambdarawgrid = np.arange(freqmin-(data.shape[1]-1)*freqstep, freqmin,freqstep)     #exp max f=393.24728523
        #lambdarawgrid = np.arange(freqmin,freqmin+(data.shape[1]-1)*freqstep,freqstep)  #exp max f=328.08615096
        #lambdarawgrid = np.arange(freqmin+(data.shape[1]-1)*freqstep,freqmin,-freqstep) #exp max f=332
        #lambdarawgrid = np.linspace(freqmin-(data.shape[1]/2)*freqstep, freqmin+(data.shape[1]/2-1)*freqstep,data.shape[1])
        lambdarawgrid = np.linspace(freqmin-(data.shape[1])*freqstep/2,freqmin,data.shape[1])    
        frawgrid = c_light/lambdarawgrid*1e-3 #1/nm/THz
 
    freqstep = float(metadata[1])*2  #FROG frequency is twice actually, but APE plots it as expected wl of the laser
    freqmax  = float(metadata[0])*2 - freqstep/2 #since the bin center of the first bin is half step shifted
    freqmin  = freqmax-freqstep*(data.shape[1]-1)
    frawgrid = np.linspace(freqmax,freqmin,data.shape[1])
    wrawgrid = c_light/frawgrid*1e-3*2 #1/nm/THz, factor 2 is added by APE to have pulse like wl
    
    if trawgrid.shape[0] != data.shape[0] or frawgrid.shape[0] != data.shape[1]:
        print("ERROR: shapes are wrong!")
  
    rawgrid = np.array(list(itertools.product(trawgrid, frawgrid)))
    
    if debug:
        plt.imshow(data.T,origin="low",extent=(trawgrid[0], trawgrid[-1], wrawgrid[0], wrawgrid[-1]),aspect="auto")
        plt.title('Raw image')
        plt.xlabel('Delay [fs]')
        plt.ylabel('Wavelenght [nm]')
        plt.show()
    
#substraction background estimated on 10% of the time scale (autocorellator scan at fixed frequency)
    for biny in range(0,data.shape[1]):
        threshold = np.average(data[0:int(0.2*data.shape[1]),biny]) 
        a = np.array([x - threshold for x in data[:,biny]])
        data[:,biny] = a
        
    if debug:
        plt.imshow(data.T,origin="low",extent=(trawgrid[0], trawgrid[-1], wrawgrid[0], wrawgrid[-1]),aspect="auto")
        plt.title('After noise substraction')
        plt.xlabel('Delay [fs]')
        plt.ylabel('Wavelenght [nm]')
        plt.show()      
    
    #new data grid definition - centred at maximum of the FROG
    maxbin = np.where(data == np.amax(data))
    tcenter = trawgrid[maxbin[0]] 
    fcenter = frawgrid[maxbin[1]]
    print("maximum bin in data raw",maxbin,tcenter,fcenter)
    tnewgrid = np.linspace(tcenter-nbin/2*deltaT,tcenter+(nbin/2-1)*deltaT,nbin)
    
    deltaF = 1/(nbin*deltaT)*1000 #in THz
    fnewgrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaT))+fcenter
    
    newgrid1, newgrid2  = np.meshgrid(tnewgrid,fnewgrid)
    
    grid_result = griddata(rawgrid, data.flatten(), (newgrid1, newgrid2), method='cubic')
    a=np.reshape(grid_result, (nbin,nbin))

#remove negative values of intensity due to cubic spline interpolation
    a = np.nan_to_num(a) #set nan values to 0
    np.clip(a, a_min=0, a_max=1e10, out=a)
    
    #center time grid at 0 for pulse plotting later
    #tnewgrid = np.linspace(-nbin/2*deltaT,(nbin/2-1)*deltaT,nbin)
    #frequency grid has center at half frequency since FROG has twice carrier frequency
    #fnewgrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaT))+fcenter/2.

    print("Raw center half frequency: ",(frawgrid.max()+frawgrid.min())/4.)
    print("Expected max frequency:    ", fcenter/2.)
    
    if debug:
        plt.figure(num=None, figsize=(4, 4), dpi=80, facecolor='w', edgecolor='k')
        #cmap=cm.gist_rainbow,
        plt.imshow(a,extent=(tnewgrid[0], tnewgrid[-1], fnewgrid[0], fnewgrid[-1]), origin='lower', aspect='auto')
        plt.title('Zoomed FROG trace')
        plt.xlabel('Delay [fs]')
        plt.ylabel('Signal frequency [THz]')
        plt.show()
    
    return a,fcenter
