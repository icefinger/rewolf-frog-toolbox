from numpy import size, mean, log, pi, zeros, max, sum, round, diff, trapz, flipud, arange, exp, angle, roll, sqrt
from numpy.fft import fft, fftshift
from numpy.random import normal, uniform, randint
import numpy as np
from makeFROG import makeFROG
from makePulse import makePulse
from draw import plotpulse, plotFROG
import scipy.optimize as opt
import matplotlib.pyplot as ppl

iterationVector = []
errorVector = []

def rmsdiffalpha(alpha,coeffs):
    return sqrt(mean((coeffs[0]-alpha*coeffs[1])**2))

def mainFROG(originalFROG, errorTolerance, maxIterations, deltaDelay, whichMethod, useBootstrap, nbFits=1):
    errorVector.clear()
    iterationVector.clear()
    (retrievedPulse, retrievedFROG, finalGError, finalIterations)=oneLoopFrog(originalFROG, errorTolerance, maxIterations, deltaDelay, whichMethod, useBootstrap)
    for i in range (1,nbFits):
        (TMPretrievedPulse, TMPretrievedFROG, TMPfinalGError, TMPfinalIterations)=oneLoopFrog(originalFROG, errorTolerance, maxIterations, deltaDelay, whichMethod, useBootstrap)
        if TMPfinalGError < finalGError:
            (retrievedPulse, retrievedFROG, finalGError, finalIterations) = (TMPretrievedPulse, TMPretrievedFROG, TMPfinalGError, TMPfinalIterations)

    return (retrievedPulse, retrievedFROG, finalGError, finalIterations)


def oneLoopFrog(originalFROG, errorTolerance, maxIterations, deltaDelay, whichMethod, useBootstrap):
    
    
    # RMS difference in the entries of two real matrices/vectors
    rmsdiff = lambda f1, f2: sqrt(mean((f1-f2)**2))



    # get trace dimensions
    N = size(originalFROG, 1)

    # normalize FROG trace to unity max intensity
    # VK: this is suggested in DeLong1996 VII.
    originalFROG = originalFROG/max(originalFROG)

    # generate initial guess
    initialIntensity = exp(-2*log(2)*((arange(0,N).T-N/2)/(N/10))**2)
    #initialPhase = exp(0.1*2*pi*1j*uniform(0,1,N))
    initialPhase = 1 #no chirp
    retrievedPulse = initialIntensity*initialPhase
    (retrievedFROG, retrievedEFROG) = makeFROG(retrievedPulse)

#   ------------------------------------------------------------
#   F R O G   I T E R A T I O N   A L G O R I T H M
#   ------------------------------------------------------------

    finalIterations = 1
    finalGError = 1e10
    testError = 1e10
    InitialTemperature = 50 #randomization temperature, in percent
    CoolingDownFactor = 0.8
    
    bestErrorRetrieved = finalGError,retrievedPulse

    while ((finalGError > errorTolerance) and (finalIterations < maxIterations)):

        #VK: This is formula (3) of DelongGP1994.
        #Or it comes from TrebinoReview1997:
        #In order to perform a GP to the FROG-trace data constraint set, 
        #it is simply necessary to replace the magnitude of EFROG with the square 
        #root of the measured FROG trace.
        #This looks to be common for all algorithms and know also as "intensitiy constraint".
        #retrievedEFROG = retrievedEFROG*(sqrt(originalFROG/retrievedFROG))
        retrievedEFROG = retrievedEFROG*(sqrt(np.divide(originalFROG, retrievedFROG, out=np.zeros_like(originalFROG), where=retrievedFROG!=0)))

        #Extract pulse field from FROG complex amplitude
        #This can be Vanilla/Basic,GP or PCGP_SVD or PCGP_Power variations
        retrievedPulse = makePulse(retrievedEFROG, retrievedPulse, whichMethod)

        # use weighted average to keep peak centered at zero
        #centerIndex = sum((arange(0, N)).T*abs(retrievedPulse**4))/sum(abs(retrievedPulse**4))
        #retrievedPulse = roll(retrievedPulse, int(-round(centerIndex-N/2)))

        # add perturbation to the pulse if the error is stagnating
        if (finalIterations%30 == 0):
            testError = finalGError

        if (abs(testError - finalGError)/testError < 0.1):
            if (bestErrorRetrieved[0]>finalGError):
                bestErrorRetrieved=finalGError,retrievedPulse
            
            maxIterations+=int(InitialTemperature)
            retrievedPulse = retrievedPulse*normal(1, InitialTemperature/100,N)
            InitialTemperature*=CoolingDownFactor

        # make a FROG trace from new fields
        (retrievedFROG, retrievedEFROG) = makeFROG(retrievedPulse)

        # calculate FROG error G, scale Fr to best match Fm, see DeLong1996,
        # and femtosoft error - intensity weighted
        #finalGError =rmsdiff(originalFROG,retrievedFROG)
        alpha0=1
        mycoeffs=[originalFROG,retrievedFROG]
        results = opt.minimize(rmsdiffalpha,alpha0,args=mycoeffs)
        finalGError=results.fun
        #retrievedEFROG*=results.x #normalise FROG E matrix to found alpha
        #print("alpha: ", results.x)

        # keeping track of error
        iterationVector.append(finalIterations)
        errorVector.append(finalGError)

        if (finalIterations % 100 == 0):
            print(f'Iteration number: {finalIterations} Error: {finalGError}')

        finalIterations = finalIterations + 1

    #Check if the last is the best
    if (bestErrorRetrieved[0]<finalGError):
        print("current error value:",finalGError,", taking back the best: ", bestErrorRetrieved[0])
        finalGError=bestErrorRetrieved[0]
        retrievedPulse=bestErrorRetrieved[1]
    print(f'Iteration number: {finalIterations} Error: {finalGError}')
    return (retrievedPulse, retrievedFROG, finalGError,  finalIterations)      

def DrawResults(originalFROG, deltaDelay, fcenter, retrievedPulse, retrievedFROG, finalGError):
    nbin = size(originalFROG, 1)
    
    # normalize FROG trace to unity max intensity
    # VK: this is suggested in DeLong1996 VII.
    originalFROG = originalFROG/np.max(originalFROG)

    # frequency interval per pixel
    #in 1000/fs = 1e3/1e-15s  = 1e12Hz = THz 
    deltaFreq = 1000/(nbin*deltaDelay) 
    print("deltaFreq is: ", deltaFreq)

    plotFROG(originalFROG,deltaDelay,fcenter,"Original FROG trace")
    plotFROG(retrievedFROG,deltaDelay,fcenter,"Reconstructed FROG trace, error= {:.6f}".format(finalGError))
    plotpulse(retrievedPulse,deltaDelay,fcenter/2)

    # retrieved spectrum   
    ppl.plot(iterationVector, errorVector, linestyle='', marker='.')
    ppl.title('Error vs iteration')
    ppl.xlabel('Iteration')
    ppl.yscale('log')

    ppl.draw()