from numpy import roll, size, fliplr, abs, ceil, outer, dot, conj
from numpy.fft import fft, fftshift, ifftshift
from matplotlib.pyplot import imshow

def makeFROG(electricField):

    N = size(electricField)

    # outer product form
    #TODO MEMORY ERROR
    electricFROG = outer(electricField, electricField)

    # row rotation
    # VK: Kane1999 - transformations can be made from the outer product form to the time domain FROG trace
    # and vice versa. This transformation can be accomplished by rotating the elements of the rows in the
    #outer product to the left by the row number minus one.
    for n in range(1, N):
        electricFROG[n, :] = roll(electricFROG[n, :], -n+1)

    # permute the columns to the right order
    # VK: This is reshuffling of the columns from Kane1999 to make 
    # tau=0 in the center, tau=1deltat to the right and so on.
    electricFROG = fliplr(fftshift(electricFROG, 1)) #VK: in matlab this is 2

    # FFT each column and put 0 frequency in the correct place:
    #electricFROG = roll(fft(electricFROG, axis=1), int(ceil(N/2)-1))
    electricFROG = fftshift(fft(ifftshift(electricFROG,axes=0), axis=0),axes=0)

    # generate FROG trace (= |field|^2)
    intensityFROG = abs(electricFROG)**2

    return (intensityFROG, electricFROG)
