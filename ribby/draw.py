import matplotlib.pyplot as ppl
import numpy as np
from scipy.optimize import curve_fit 

# fit chirp as a parabola
def fit_chirp(x, c, b, a): 
    return c*x**2 + b*x +a

#phase unwrapping
#We shift first the central phase to 0 
#then to left and to right if the difference between the next points is > 3 (~pi) we add 2Pi
def unwrapphase(_phasetime, _timeLabels, _intensitytime):   
    N = _phasetime.shape[0]
    phasetimetemp = _phasetime-_phasetime[int(N/2)]
    
    for i in range(int(N/2),len(phasetimetemp)-1):
        diff=phasetimetemp[i]-phasetimetemp[i+1]
        if diff < -3:
            for j in range(i+1, len(phasetimetemp)):
                phasetimetemp[j]-=2*np.pi
        if diff > 3:
            for j in range(i+1, len(phasetimetemp)):
                phasetimetemp[j]+=2*np.pi
    
    for i in range(int(N/2),0,-1):
        diff=phasetimetemp[i]-phasetimetemp[i-1]
        if diff < -3:
            for j in range(i-1,-1,-1):
                phasetimetemp[j]-=2*np.pi
        if diff > 3:
            for j in range(i-1,-1,-1):
                phasetimetemp[j]+=2*np.pi
    
    phasetimetemp=np.where(_intensitytime<0.03,1e9,phasetimetemp)
    
    (tmpphase, tmptime, tmpint) = (phasetimetemp.tolist(), _timeLabels.tolist(), _intensitytime.tolist())
    
    
    while tmpphase.count(1e9) != 0:
        index=tmpphase.index(1e9)
        tmpphase.pop(index)
        tmptime.pop(index)
        tmpint.pop(index)
    
                
    return (np.array(tmpphase), np.array(tmptime),np.array(tmpint) ) 

#pulsefcenter - 0(central) frequency of the pulse (carrier frequency)
def plotpulse(pulsetime,deltaDelay,pulsefcenter):
    #in 1000/fs = 1e3/1e-15s  = 1e12Hz = THz 
    nbin = pulsetime.shape[0]
    deltaFreq = 1000/(nbin*deltaDelay) 
    tpulsegrid = np.linspace(-nbin/2*deltaDelay,(nbin/2-1)*deltaDelay,nbin)
    fpulsegrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaDelay))+pulsefcenter
    
    intensitytime=abs(pulsetime)**2/max(abs(pulsetime))**2
    phasetime=np.angle(pulsetime)
    (cleanphase, cleantime, cleanintensitytime) = unwrapphase(phasetime,tpulsegrid,intensitytime)
    fig, ax1 = ppl.subplots()
    ppl.title('temporal')
    color = 'tab:blue'
    ax1.set_xlabel('Time [fs]')
    ax1.set_ylabel('Intensity [a.u.]', color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.plot(tpulsegrid, intensitytime,marker=".")
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:red'
    ax2.set_ylabel('phase [rad]', color=color)  # we already handled the x-label with ax1
    ax2.plot(cleantime,cleanphase,color="red", marker=".")
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    ppl.show()

    # retrieved spectrum
    FFTPt = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(pulsetime)))
    intensityfreq=abs(FFTPt)**2/max(abs(FFTPt))**2
    phasefreq = np.angle(FFTPt)
    (cleanphasefreq, cleanfreq, cleanintfreq) = unwrapphase(phasefreq,fpulsegrid,intensityfreq)

    fig, ax1 = ppl.subplots()
    ppl.title('spectral')
    color = 'tab:blue'
    ax1.set_xlabel('Frequency [THz]')
    ax1.set_ylabel('Intensity [a.u.]', color=color)
    ax1.plot(fpulsegrid, intensityfreq, marker=".")
    ax1.tick_params(axis='y', labelcolor=color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:red'
    ax2.set_ylabel('phase [rad]', color=color)  # we already handled the x-label with ax1
    ax2.plot(cleanfreq,cleanphasefreq,color=color, marker=".")
    param2, param_cov2 = curve_fit(fit_chirp, cleanfreq, cleanphasefreq) 
    ax2.plot(cleanfreq, fit_chirp(cleanfreq, param2[0],param2[1],param2[2]), color="tab:green", label="chirp={:.6f} fs^2".format(np.abs(param2[0]*1e6/2./(np.pi**2))))
    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    ppl.legend(loc="best")
    ppl.show()

    #this is calculated as number of points per deltaT/deltaF... 
    #spline aprroximation and calc about half can improve the precision
    print("bandwidth: ",len(intensityfreq[intensityfreq>=0.5])*deltaFreq,"THz (intensity FWHM)")
    print("duration:  ",len(intensitytime[intensitytime>=0.5])*deltaDelay,"fs (intensity FWHM)")       
    #Taylor series for spectral phase in 
    #Femtosecond Laser Pulses: Linear Properties, Manipulation, Generation and Measurement 
    #Matthias Wollenhaupt, Andreas Assion and Thomas Baumert 
    #phi(omega)=phi(omega_0)+phi'(omega_0)(omega-omega_0)+1/2*phi''(omega-omega_0)^+...
    #phi'' may be referred as "chirp". Note, that GDD introduces chirp with phi''=GDD!
    #phi(f)=c*f^2+b^f+a
    #phi(omega)=c*(omega/2Pi)^2+...=c/(2Pi)^2*omega
    #1/2*phi''=c/(2Pi)^2
    #phi''=c/2/Pi^2
    print("chirp phi''", np.abs(param2[0]*1e6/2./(np.pi**2)),"fs^2")
    print("max frequency: ", fpulsegrid[np.where(intensityfreq == 1)])

#fcenter - 0(central) frequency of the FROG image (double carrier frequency)
def plotFROG(FROG,deltaDelay,fcenter,title):
    nbin = FROG.shape[0]
    #in 1000/fs = 1e3/1e-15s  = 1e12Hz = THz 
    deltaFreq = 1000/(nbin*deltaDelay) 
    tfroggrid = np.linspace(-nbin/2*deltaDelay,(nbin/2-1)*deltaDelay,nbin)
    ffroggrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaDelay))+fcenter
    ppl.imshow(np.sqrt(FROG)*64, extent=(tfroggrid[0], tfroggrid[-1], ffroggrid[0], ffroggrid[-1]), origin='lower', aspect='auto') #64 is colormap range, sqrt for electric field in matlab
    ppl.title(title)
    ppl.xlabel('Delay [fs]')
    ppl.ylabel('Signal frequency [THz]')
    ppl.show()
